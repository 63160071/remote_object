/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.remote;

/**
 *
 * @author Maintory_
 */
public class Remote {
    private int temp;
    private String name;
    private int fan;
    public void setName(String name){
        this.name = name;
    }
    public void setTemp(int temp){
        this.temp = temp;
    }
    public void setFan(int fan){
        this.fan = fan;
    }
    public int upTemp(){
        if(temp >= 30){
            System.out.println(getName() + " Cannot go further than 30 C  \n");
        }
        else{
        temp = temp + 1;
        }
        return temp;
    }
    public int downTemp(){
        if(temp <= 18){
            System.out.println(getName() + " Cannot go below than 18 C  \n");
        }
        else{
        temp = temp - 1;
        }
        return temp;
    }
    public int upFan(){
        if(fan >= 3) {
            fan = 1;
        }
        else{
            fan++;
        }
        return fan;
    }
    public void tellRemote(){ //to introduce the information of remote control pannel
        System.out.println("Beep!!");
        System.out.println("Name: " +getName());
        System.out.println("Temp: " +getTemp() + " C");
        System.out.println("Fan: " +getFan() +"\n");
    }
    public String getName(){
    return name;
    }
    public int getTemp(){
    return temp;
    }
    public int getFan(){
    return fan;
    }
}
