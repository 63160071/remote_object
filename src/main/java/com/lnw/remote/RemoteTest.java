/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.remote;

/**
 *
 * @author Maintory_
 */
public class RemoteTest {
    public static void main(String[] args) {
        //------------------------------------------------------------------//
                            // SET THE DEFAULT VALUE //
       //------------------------------------------------------------------//
        Remote r1 = new Remote(); //constructor r1
        Remote r2 = new Remote(); //constructor r2
        r1.setName("r1"); //setname of the remote
        r2.setName("r2");
        r1.setTemp(28); //Set the default temperature of r1 at 28 degree celcius
        r1.setFan(1);
        r2.setTemp(19); //Set the default temperature of r1 at 19 degree celcius
        r2.setFan(3);
        //------------------------------------------------------------------//
                          // END OF SET THE DEFAULT VALUE //
       //------------------------------------------------------------------//
        
       
        //------------------------------------------------------------------//
                              // PUSHING THE BUTTON //
       //------------------------------------------------------------------//
        r1.tellRemote(); //tell the information of remote
        r1.upTemp();
        r1.upTemp();
        r1.upTemp();
        r1.upTemp();
        r1.tellRemote();
        //getTemp(); is the getter of Temperature in Remote Class
        //push the temp up button
        r2.tellRemote(); //down the temp at 1 degree
        r2.downTemp(); //up temp at 1 celcius degree
        r2.downTemp(); //up temp at 1 celcius degree
        r2.downTemp(); //up temp at 1 celcius degree
        r2.tellRemote(); //down the temp at 1 degree
        r2.upFan();
        r2.tellRemote(); //down the temp at 1 degree
        r2.upFan();
        r2.tellRemote(); //down the temp at 1 degree
    }
}
